#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Arc
import binascii
import qam16 as qam
import qam4
from aes import init, get_next,rotate_table_dec,generate_random_complex,rotate_table
import argparse
#show,input,dec

###Parser
parser = argparse.ArgumentParser(
    description='This program is for read encrypted IQ samples for a message from a file!')
parser.add_argument('--show', action='store_true',
                    help='if present, show the constelations of symbols and what they decrypt to')
parser.add_argument('--dec', action='store_true',
                    help='if present, decrypts the IQ values')
parser.add_argument('--norm', action='store_true',
                    help='if present, then decryption will normalize the random blocks')
parser.add_argument('--unshuffle', action='store_true',
                    help='if present, then decryption will unshuffle the symbols for decryption')
parser.add_argument('--qam', metavar='QAM', type=str, default=None,
                    help='QAM config to use, default is qam16.py. Note do no include ".py"')
parser.add_argument('-f', metavar='FILE', type=str, default='samples.iq',
                    help='file to read from, default samples.iq')

###TODO:
# key

args = parser.parse_args()
args = vars(args)

FILE = args['f']
show = args['show']
dec = args['dec']
key = b'\0'*16
norm = args['norm']
unshuffle = args['unshuffle']

if (args['qam']):
    qam = __import__(args['qam'])

###Setup
# take the existing table and create the inverse
demap_table16 = {v : k for k, v in qam.symbols.items()}
demap_table4 = {v : k for k, v in qam4.symbols.items()}

###Helpers
flatten = lambda l: [item for sublist in l for item in sublist]
to_byte = lambda x: chr(int('0b'+x,2))
to_by = lambda x: int('0b'+x,2)

# demapping function for when the noise interferes with transmission
# see sources, Basic OFDM Example in Python
def demapping(syms, demap_table,dec):
    constellation = np.array([x for x in demap_table.keys()])
    dists = abs(syms.reshape((-1,1)) - constellation.reshape((1,-1)))
    const_index = dists.argmin(axis=1)
    hardDecision = constellation[const_index]
    if(dec and unshuffle):
        hardDecision = unshuffle_syms(hardDecision,qam.symbols.values())
    return np.vstack([demap_table[C] for C in hardDecision]), np.asarray(hardDecision)

def unshuffle_syms(symbols,table):
    mapping = dict(zip(table,table))
    syms = []
    for s in symbols:
        mapping = rotate_table(get_next(),mapping)
        yeet = {v : k for k, v in mapping.items()}
        syms.append(yeet[s])

    return syms



###Main
# read the encrypted samples from an iq file
msgsymbols = np.fromfile(FILE, dtype=np.complex64)

# remove the beginning (transient) part (its the length of our
# filter/2 plus one for that extra sample we started with)
qW = msgsymbols[75+1:] 
msgsymbols = qW[::8] # only save 1 out of every 8 samples (decimate)

# decrypt, ignoreing the preamble, keep the old symbols for graphing
msgencsymbols = msgsymbols[len(qam4.preamble):]

###Decompose Header
# See spec for details on header
headersyms = msgencsymbols[:80]
bit_arr,y = demapping(headersyms, demap_table4,False)
bits = "".join(map(str,flatten(bit_arr)))
iv_bits = bits[:128]
size_bits = bits[128:]

#handle iv since binascii ignores null bytes
iv_bits = [iv_bits[i:i+8] for i in range(0, len(iv_bits), 8)]
iv_bits = map(to_byte, iv_bits)
iv = "".join(iv_bits)
#print(iv)

size = int(binascii.unhexlify("%x" % (int('0b'+size_bits,2))))

###Set up for decrypt
init(iv,key) #AES128, IV=0 (16bytes), key =0 (16bytes)

###Decrypt

if (dec):
    if (norm):
        norm = (lambda x: x/np.abs(x))
        app = np.vectorize(lambda x: x / norm(generate_random_complex(get_next())))
    else:
        app = np.vectorize(lambda x: x / generate_random_complex(get_next()))
    msgsymbols = app(msgencsymbols[80:80+size])
else:
    msgsymbols = msgencsymbols[80:80+size]

###Demapp and plot graphs
# demapp and graph the old point vs decoded-mapped symbols

#msg_bits, y = demapping(msgsymbols, demap_table16)
init(iv,key)

msg_bits, y = demapping(msgsymbols,demap_table16,dec)

if (show):
    for qam, hard in zip(msgencsymbols[80:80+size], y): # enc vs dec-mapping
        plt.plot([qam.real, hard.real], [qam.imag, hard.imag], 'b-o');
    plt.plot(y.real, y.imag, 'ro')

###Reassemble the message
msg_bits = "".join(map(str,flatten(msg_bits)))
#print(msg_bits)


msg_bits = int('0b'+msg_bits,2)

print(binascii.unhexlify("%x" % msg_bits))

if (show):
    plt.show()
