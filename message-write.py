#!/usr/bin/env python
import numpy as np
import qam16 as qam
import qam4
from Crypto import Random
from aes import init, get_next, generate_random_complex,rotate_table
from RCPSFilter import make_filter, print_info
import argparse

HEADER_SIZE = 4
IV_SIZE = 16

###Parser
parser = argparse.ArgumentParser(
    description='This program is for writing encrypted IQ samples for a message to a file!')
parser.add_argument('--taps', metavar='N', type=int, default=151,
                    help='number of taps to use in the FIR filter. default is 151')
parser.add_argument('--alpha', metavar='a', type=float, default=0.35,
                    help='alpha for the FIR filter. default is 0.35')
parser.add_argument('--sam_per_sym', metavar='S', type=int, default=8,
                    help='number of samples per sybmol for filtering')
parser.add_argument('--enc', action='store_true',
                    help='if present, then encryption will take place')
parser.add_argument('--norm', action='store_true',
                    help='if present, then encryption will normalize the random blocks')
parser.add_argument('--shuffle', action='store_true',
                    help='if present, then encryption will shuffle map the symbols')
parser.add_argument('--noise', action='store_true',
                    help='if present, then noise will be applied to the signal')
parser.add_argument('--zeroiv', action='store_true',
                    help='if present, then encryption will use a zero iv')
parser.add_argument('--iv', metavar='IV', type=str, default=None,
                    help='if present, then encryptio')
parser.add_argument('--show', action='store_true',
                    help='if present, then graph will be shown')
parser.add_argument('-o', metavar='FILE', type=str, default='samples.iq',
                    help='file to write to')
parser.add_argument('--qam', metavar='QAM', type=str, default=None,
                    help='QAM config to use, default is qam16.py. Note do no include ".py"')
parser.add_argument('-m', metavar='MSG', type=str, required=True,
                    help='the message to be encrypted and sent')

###TODO:
# noise
# key
# normalize random?

args = parser.parse_args()
args = vars(args)

TAPS = args['taps']
ALPHA = args['alpha']
SAM_PER_SYM = args['sam_per_sym']
OUTFILE = args['o']
msg = args['m']
enc = args['enc']
show = args['show']
norm = args['norm']
shuffle = args['shuffle']
zero_iv = args['zeroiv']
iv_b = args['iv']
key = b'\0'*16
noise = args['noise']

if (args['qam']):
    qam = __import__(args['qam'])

def shuffle_syms(symbols,table):
    mapping = dict(zip(table,table))
    syms = []
    for s in symbols:
        mapping = rotate_table(get_next(),mapping)
        syms.append(mapping[s])

    return syms

###Convert to binary
def convert_to_binary(msg, sym_size): 
    msgbits = []
    for byte in msg:
        byte = ord(byte)
        for bit in range(7, -1, -1): # MSB
            msgbits.append((byte >> bit) % 2)

        # fix to be a multiple of symbol size
        while len(msgbits) % sym_size:
            msgbits.append(0)
    return msgbits

###Translate 
def translate_to_syms(msgbits, symbols, sym_size):
    msgsymbols = []

    # translate everything to complex IQ values
    for n in range(0, len(msgbits), sym_size):
        symbol = symbols[tuple(msgbits[n:n+sym_size])]
        #x,y = np.random.uniform(-1,1,[2])
        msgsymbols.append(symbol) #+ (x+y*1j))

    #correct the type for file writing
    msgsymbols = np.asarray(msgsymbols, dtype=np.complex64)
    return msgsymbols


###ENC
if (iv_b):
    iv = iv_b.zfill(16)
elif (zero_iv):
    iv = b'\0'*16
else:
    iv = Random.get_random_bytes(16)
#print(iv)

# setup for applying the encrytion to the symbols

###MAIN
msgbits = convert_to_binary(msg, qam.SYMBOL_SIZE)

msgsymbols = translate_to_syms(msgbits, qam.symbols, qam.SYMBOL_SIZE)

#save for later
plaintext = msgsymbols

###Shuffling
init(iv, key)                          # AES128, IV=0 (16bytes), key =0 (16bytes)
if (enc and shuffle):
    msgsymbols = shuffle_syms(msgsymbols,qam.symbols.values())

init(iv, key)                          # AES128, IV=0 (16bytes), key =0 (16bytes)
# app encryption
if (enc):
    if (norm):
        norm = (lambda x: x/np.abs(x))
        app = np.vectorize(lambda x: x * norm(generate_random_complex(get_next())))
    else:
        app = np.vectorize(lambda x: x * generate_random_complex(get_next()))
    msgsymbols = app(msgsymbols)

###Make Header
size = len(msgsymbols)
size = str(size).zfill(4)
size_bits = convert_to_binary(size,qam4.SYMBOL_SIZE)
size_syms = translate_to_syms(size_bits, qam4.symbols, qam4.SYMBOL_SIZE)

iv_bits = convert_to_binary(iv,qam4.SYMBOL_SIZE)
iv_syms = translate_to_syms(iv_bits, qam4.symbols, qam4.SYMBOL_SIZE)

###Preamble: add the preamble after encryption
header = np.append(qam4.preamble, iv_syms)
header = np.append(header, size_syms)
msgsymbols = np.append(header, msgsymbols)
plaintext = np.append(header, plaintext)

#msgsymbols = np.append(qam4.preamble, msgsymbols)
#plaintext = np.append(qam4.preamble, plaintext)

###Filtering
# Encrypted Samples per symbol
padded = np.asarray([0], dtype=np.complex64)
for sym in msgsymbols:
    padded = np.append(padded, sym)
    padded = np.append(padded, np.zeros(SAM_PER_SYM - 1))
msgsymbols = padded

# force complex64
msgsymbols = np.asarray(msgsymbols, dtype=np.complex64)

###Actually filter
fil = make_filter(SAM_PER_SYM, TAPS, ALPHA)
qW = np.convolve(fil, msgsymbols) # Waveform with PSF

##Write to file
qW = np.asarray(qW, dtype=np.complex64)

if(noise):
    for ind,s in enumerate(qW):
        x = (np.random.normla() + 1j * np.random.randn())/8
        print(x)
        qW[ind] += x

qW.tofile(OUTFILE)

if (show):
    print_info(qW)

