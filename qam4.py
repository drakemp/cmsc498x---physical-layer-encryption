import numpy as np

SYMBOL_SIZE = 2

symbols = {
    (0,0) :  -1-1j,   
    (1,0) :   1-1j,
    (0,1) :  -1+1j,
    (1,1) :   1+1j,
}

preamble = np.asarray( [1-1j,-1+1j,1-1j,-1+1j,1-1j,-1+1j,1-1j,-1+1j,1-1j,-1+1j], dtype=np.complex64)

