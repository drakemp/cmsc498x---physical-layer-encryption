from Crypto.Cipher import AES
from Crypto.Util import Counter
import struct
import numpy as np
import random

ctr = None
cipher = None

def init(iv, key):
    global ctr
    global cipher
    ctr = Counter.new(128, initial_value=long(iv.encode('hex'), 16))
    cipher = AES.new(key, AES.MODE_CTR, counter=ctr)

def rotate_table(r,table):
    random.seed(r)
    tval = table.values()
    vals = random.sample(tval,len(tval))
    return dict(zip(table.keys(),vals))

def rotate_table_dec(r,table):
    random.seed(r)
    tval = table.keys()
    vals = random.sample(tval,len(tval))
    return dict(zip(vals,table.values()))
    

def generate_random_complex(r):
    a = lambda t: struct.unpack('f', t)
    I = lambda t: struct.unpack('i', t)
    i_n,i_d = I(r[0:4])[0],I(r[4:8])[0]
    q_n,q_d = I(r[8:12])[0],I(r[12:16])[0]
    i = i_n / (i_d*1.0)
    q = q_n / (q_d*1.0)
    return np.complex64(i+1j*q)

def get_next():
    global cipher
    r = cipher.encrypt(b'\0'*16)
    return r
    
    
