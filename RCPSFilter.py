import numpy as np
import matplotlib.pyplot as plt
### Create Raised Cosine (Pulse Shaping) Filter ###

def make_filter(samples_per_sym, num_taps, beta):
    Ts = samples_per_sym # number of samples per symbol
    N = num_taps         # number of filter taps to generate
    alpha = beta         # roll-off factor (aka beta)
    h_rc = np.zeros(N, dtype=float) # this will be the filter taps
    for x in np.arange(N):
        t = (x-N/2)
        if t == 0.0:
            h_rc[x] = 1.0
        elif alpha != 0 and t == Ts/(2*alpha):
            h_rc[x] = (np.pi/4)*(np.sin(np.pi*t/Ts)/(np.pi*t/Ts))
        elif alpha != 0 and t == -Ts/(2*alpha):
            h_rc[x] = (np.pi/4)*(np.sin(np.pi*t/Ts)/(np.pi*t/Ts))
        else:
            h_rc[x] = (np.sin(np.pi*t/Ts)/(np.pi*t/Ts))*(np.cos(np.pi*alpha*t/Ts)/(1-(((2*alpha*t)/Ts)*((2*alpha*t)/Ts))))
    return h_rc

def print_info(h_rc):
    plt.plot(h_rc,'.-')
    plt.show()
